package com.example.tesorocaza;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class task123 extends AppCompatActivity {

    EditText a;
    String z="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task123);
        a = (EditText) findViewById(R.id.editText4);
    }
    public  void read(View view)
    {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setResultDisplayDuration(0);//Text..
        integrator.setScanningRectangle(450, 450);
        integrator.setCameraId(0);
        integrator.initiateScan();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result != null)
        {
            if(result.getContents()==null)
            {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
            else
            {

                a.setText(result.getContents());
                z = a.getText().toString();

            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
    public  void check(View view)
    {
        z = a.getText().toString();
        if(z.equals("0"))
        {
            Toast.makeText(this, "Scan the QR code", Toast.LENGTH_SHORT).show();
        }
        else if(z.equals("wt5c9IS5M3Qg"))
        {
            Intent intent = new Intent(this,taskfinal.class);
             startActivity(intent);
        }
        else
        {
            Toast.makeText(this, "Wrong key", Toast.LENGTH_SHORT).show();
        }
    }
}
